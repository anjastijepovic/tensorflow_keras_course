from flask import Flask, render_template, session, url_for, redirect
from flask_wtf import FlaskForm
import numpy as np
from wtforms import TextAreaField, SubmitField, FloatField
from tensorflow.keras.models import load_model
import joblib


# the return prediction funct takes in the model, scaler,and the sample data passed to it

def return_prediction(model, scaler, sample_json):

  # For larger data features, you should probably write a for loop
  # That builds out this array for you
  s_len = sample_json["sepal_length"]
  s_wid = sample_json["sepal_width"]
  p_len = sample_json["petal_length"]
  p_wid = sample_json["petal_width"]

  flower = [[s_len, s_wid, p_len, p_wid]]

  classes = np.array(['setosa', 'versicolor', 'virginica'])

  # we need to scale the flower data so it could be passed to the model
  flower = scaler.transform(flower)

  # class_ind = model.predict_classes(flower)[0]
  # predict_classes ne moze vise nego samo predict koji daje probabilities za svaku klasu

  # Predict the probabilities for each class
  predictions = model.predict(flower)

  # Get the index of the class with the highest probability
  class_ind = np.argmax(predictions, axis=1)[0]
  # [0] da bi uzeo indeks (broj samo) predvidjene klase a ne array

  return classes[class_ind]
  # return ime klase sa indeksom class_ind, npr 'setosa' ima ind 0


flower_model = load_model('iris_model_anja.h5')
flower_scaler = joblib.load('iris_scaler_anja.pkl')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecretkey'

class FlowerForm(FlaskForm):

    # what the user will see at the html page
    sep_len = FloatField("Sepal Length")
    sep_wid = FloatField("Sepal Width")
    pet_len = FloatField("Petal Length")
    pet_wid = FloatField("Petal Width")

    submit = SubmitField("Analyze")


@app.route("/", methods=['GET','POST'])
# homepage. it has the form
def index():
    
    form = FlowerForm() #instance of the FlowerForm class. it will grab the text from the form

    if form.validate_on_submit():

        session['sep_len'] = form.sep_len.data
        session['sep_wid'] = form.sep_wid.data
        session['pet_len'] = form.pet_len.data
        session['pet_wid'] = form.pet_wid.data

        return redirect(url_for('prediction'))
        # when the user submits the data we wanna redirect them to the flower prediction page
    return render_template('home.html', form=form)

@app.route("/prediction")
def prediction():

    content = {}

    content['sepal_length'] = float(session['sep_len'])
    content['sepal_width'] = float(session['sep_wid'])
    content['petal_length'] = float(session['pet_len'])
    content['petal_width'] = float(session['pet_wid'])

    results = return_prediction(flower_model, flower_scaler, content)

    return render_template('prediction.html', results=results)



if __name__=='__main__':
    app.run()
